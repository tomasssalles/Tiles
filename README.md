# Tiles

This is a very simple gaminig app for iOS that I decided to write in order to help myself define my own coding standards by trying out different techniques that people swear by.

<div style="text-align: center; font-size:85%; width: 300px; margin: auto">
<img src="readme_game_example.png" alt="game_example" style="height: 200px; margin: auto; display: block;"/>
I hope this picture is sufficient to replace an explanation of the game the app implements and why the project is called "Tiles".
</div>
