//
//  AppDelegate.swift
//  Tiles
//
//  Created by Tomás Silveira Salles on 22.10.17.
//  Copyright © 2017 Tom GmbH. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey : Any]? = nil) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        let viewController = UIViewController()
        window!.rootViewController = viewController
        window!.makeKeyAndVisible()
        return true
    }
}
